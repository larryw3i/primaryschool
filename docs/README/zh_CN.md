
<h1 align="center">
  <br>
  primaryschool
  <br>
</h1>

<h4 align="center">
    小学知识游戏。
</h4>

<p align="center">
    <a href="https://badge.fury.io/py/primaryschool">
        <img 
            src="https://badge.fury.io/py/primaryschool.svg" 
            alt="PyPI version" height="18" />
    </a>
</p>

<p align="center">
  <a href="#安装">安装</a> 
  • 
  <a href="#待办">待办</a> 
  • 
  <a href="#声明">声明</a> 
  • 
  <a href="#授权">授权</a>
</p>  

<img 
    src="../images/screenshot.0.png" 
    alt="screenshot" 
    width="100%" 
    height="auto"/>

## 安装

安装 
[primaryschool](https://pypi.org/project/primaryschool), 
你的计算机上需要装有 
[Python3](https://www.python.org/downloads/) 和 
[python3-pip](https://pypi.org/) 
(默认是 [Python3](https://www.python.org/downloads/) 自带的, 
Unix-like 系统可能需要单独安装) 
。 然后在命令行或终端输入:
```bash
pip3 install -U primaryschool; primaryschool # 回车。
```  
你也可以使用镜像站点加速网络流量:  
```bash
pip3 install -U -i https://mirrors.bfsu.edu.cn/pypi/web/simple primaryschool;  
primaryschool # 回车。
```  

## 待办
- [ ] 游戏搜索

## 声明

此项目依赖以下的项目:  
- [pygame](https://github.com/pygame/pygame)
- [pygame-menu](https://github.com/ppizarror/pygame-menu)
- [xpinyin](https://github.com/lxneng/xpinyin)
- [appdirs](https://github.com/ActiveState/appdirs)


## 授权

[MIT License](https://github.com/larryw3i/primaryschool/blob/daily/LICENSE)

---

