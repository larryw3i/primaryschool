
<h1 align="center">
  <br>
  primaryschool
  <br>
</h1>

<h4 align="center">
    primary school knowledge games.
</h4>

<p align="center">
    <a href="https://badge.fury.io/py/primaryschool">
        <img 
            src="https://badge.fury.io/py/primaryschool.svg" 
            alt="PyPI version" height="18" />
    </a>
</p>

<p align="center">
  <a href="#how-to-use">How To Use</a> 
  • 
  <a href="#todo-list">Todo List</a> 
  • 
  <a href="#credits">Credits</a> 
  • 
  <a href="#license">License</a>
</p>  

<img 
    src="https://github.com/larryw3i/primaryschool/blob/daily/docs/images/screenshot.0.png?raw=true" 
    alt="screenshot" 
    width="100%" 
    height="auto"/>

## How To Use

To run 
[primaryschool](https://pypi.org/project/primaryschool), 
you'll need 
[Python3](https://www.python.org/downloads/) and 
[python3-pip](https://pypi.org/) 
(which comes with [Python3](https://www.python.org/downloads/) generally, 
Unix-like may need to install separately.) 
installed on your computer. From your command line:

```bash
pip3 install -U primaryschool; primaryschool
```  

## Todo List
- [ ] Games search

## Credits

This software uses the following open source packages:

- [pygame](https://github.com/pygame/pygame)
- [pygame-menu](https://github.com/ppizarror/pygame-menu)
- [xpinyin](https://github.com/lxneng/xpinyin)
- [appdirs](https://github.com/ActiveState/appdirs)


## License

[MIT License](https://github.com/larryw3i/primaryschool/blob/daily/LICENSE)

---

